from tagger import FileTagsInfoPresenter


def test_file_tags_info_presenter_for_empty_tag_info():
    assert (
        FileTagsInfoPresenter(file_name="a1.wav", tags_info="").present()
        == "[a1.wav] n/a"
    )


def test_file_tags_info_presenter_replaces_tag_names():
    assert (
        FileTagsInfoPresenter(
            file_name="a1.wav",
            tags_info="tracktitle: hello\ntracknumber: 1\ntotaltracks: 108",
        ).present()
        == """[a1.wav] title: hello
track: 1
tracks: 108"""
    )
