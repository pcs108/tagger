import pytest

from tagger import (
    TagInputParser,
    TagNameParser,
    TagNameValidator,
    TagValueParser,
    TagValueValidator,
)


def test_tag_name_validator_when_tag_name_is_accepted():
    assert TagNameValidator("tracktitle").validate() is True


def test_tag_name_validator_when_tag_name_is_not_accepted():
    with pytest.raises(TagNameValidator.InvalidTagName):
        TagNameValidator("foobar").validate()


def test_tag_value_validator_when_tag_name_and_value_are_accepted():
    assert TagValueValidator(tag="tracknumber", value="123").validate() is True


def test_tag_value_validator_when_tag_name_is_accepted_but_value_is_not():
    with pytest.raises(TagValueValidator.InvalidTagValue):
        TagValueValidator(tag="tracknumber", value="foobar").validate()


def test_tag_value_validator_when_tag_name_is_not_accepted():
    with pytest.raises(TagValueValidator.InvalidTagValue):
        TagValueValidator(tag="foobar", value="hello").validate()


def test_tag_name_parser_for_valid_input_with_no_changes_needed():
    assert TagNameParser("album").parse() == "album"


def test_tag_name_parser_for_valid_input_with_changes_needed():
    assert TagNameParser("al_bum").parse() == "album"


def test_tag_name_parser_raises_error_for_invalid_input():
    with pytest.raises(TagNameValidator.InvalidTagName):
        TagNameParser("foo_bar").parse()


def test_tag_value_parser_for_valid_tag_and_value():
    assert TagValueParser({"artist": "foobie"}).parse() == {"artist": "foobie"}


def test_tag_value_parser_normalizes_tag_name():
    assert TagValueParser({"arti_st": "foobie"}).parse() == {"artist": "foobie"}


def test_tag_value_parser_raises_error_for_invalid_dict_input():
    with pytest.raises(TagValueParser.InvalidTagInfo):
        TagValueParser({"artist": "foobie", "album": "foobie"})


def test_tag_value_parser_raises_error_for_invalid_tag_name():
    with pytest.raises(TagNameValidator.InvalidTagName):
        TagValueParser({"foobar": "hello"}).parse()


def test_tag_value_parser_raises_error_for_invalid_tag_value():
    with pytest.raises(TagValueValidator.InvalidTagValue):
        TagValueParser({"tracknumber": "foobar"}).parse()


def test_tag_input_parser_for_valid_input():
    assert TagInputParser("tracknumber=123").parse() == {"tracknumber": "123"}


def test_tag_input_parser_normalizes_tag_name():
    assert TagInputParser("track_number=123").parse() == {"tracknumber": "123"}


def test_tag_input_parser_raises_error_for_invalid_tag_name():
    with pytest.raises(TagNameValidator.InvalidTagName):
        TagInputParser("foobar=hello").parse()


def test_tag_input_parser_raises_error_for_invalid_tag_value():
    with pytest.raises(TagValueValidator.InvalidTagValue):
        TagInputParser("tracknumber=foobar").parse()
