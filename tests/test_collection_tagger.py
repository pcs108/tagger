import music_tag

from tagger import CollectionTagger, FileTagger, TagInputParser, TagValueParser

from . import AUDIO1, AUDIO2, AUDIO3, TEST_DATA


def test_collection_tagger_parse_applied_tags_applies_parser_to_user_input():
    assert CollectionTagger.parse_applied_tags(
        ["artist=foobar", "genre=hello"], parser=TagInputParser
    ) == {"artist": "foobar", "genre": "hello"}


def test_collection_tagger_parse_applied_tags_applies_parser_to_tag_dicts():
    assert CollectionTagger.parse_applied_tags(
        [{"artist": "foobar"}, {"genre": "hello"}], parser=TagValueParser
    ) == {"artist": "foobar", "genre": "hello"}


def test_collection_tagger_parse_removed_tags_parses_tag_names():
    assert CollectionTagger.parse_removed_tags(["art_ist", "album"]) == [
        "artist",
        "album",
    ]


def test_collection_tagger_from_files_and_tags():
    txt_file = TEST_DATA / "invalid.txt"
    txt_file.write_text("")

    collection = CollectionTagger.from_files_and_tags(
        files=[AUDIO1, txt_file, AUDIO2, TEST_DATA, AUDIO3],
        applied_tags=[
            {"genre": "Punk Rock"},
            {"album": "Britney Spears Greatest Hits"},
        ],
        removed_tags=["artist", "track_number"],
        assign_track_numbers=True,
    )

    assert collection == CollectionTagger(
        file_taggers=[
            FileTagger.from_file(AUDIO1),
            FileTagger.from_file(AUDIO2),
            FileTagger.from_file(AUDIO3),
        ],
        applied_tags={"genre": "Punk Rock", "album": "Britney Spears Greatest Hits"},
        removed_tags=["artist", "tracknumber"],
        assign_track_numbers=True,
    )


def test_collection_tagger_from_files_and_user_inputs():
    txt_file = TEST_DATA / "invalid.txt"
    txt_file.write_text("")

    collection = CollectionTagger.from_files_and_user_inputs(
        files=[AUDIO1, txt_file, AUDIO2, TEST_DATA, AUDIO3],
        applied_tags_input=["genre=Punk Rock", "album=Britney Spears Greatest Hits"],
        removed_tags_input=["artist", "track_number"],
        assign_track_numbers=True,
    )

    assert collection == CollectionTagger(
        file_taggers=[
            FileTagger.from_file(AUDIO1),
            FileTagger.from_file(AUDIO2),
            FileTagger.from_file(AUDIO3),
        ],
        applied_tags={"genre": "Punk Rock", "album": "Britney Spears Greatest Hits"},
        removed_tags=["artist", "tracknumber"],
        assign_track_numbers=True,
    )


def test_collection_tagger_get_info_returns_info():
    a1 = FileTagger.from_file(AUDIO1)
    a2 = FileTagger.from_file(AUDIO2)
    a3 = FileTagger.from_file(AUDIO3)

    collection = CollectionTagger(file_taggers=[a1, a2, a3])

    assert collection.get_info() == f"{a1.info}\n{a2.info}\n{a3.info}"


def test_collection_tagger_remove_unsets_specified_tags():
    a1 = music_tag.load_file(AUDIO1)
    a1["artist"] = "Backstreet Boys"
    a1["genre"] = "Pop"
    a1.save()

    a2 = music_tag.load_file(AUDIO2)
    a2["artist"] = "Backstreet Boys"
    a2.save()

    a3 = music_tag.load_file(AUDIO3)
    a3["artist"] = "Backstreet Boys"
    a3["genre"] = "Pop"
    a3.save()

    collection = CollectionTagger(
        file_taggers=[
            FileTagger.from_file(AUDIO1),
            FileTagger.from_file(AUDIO2),
            FileTagger.from_file(AUDIO3),
        ],
        removed_tags=["genre"],
        applied_tags={"album": "Backstreet Boys Greatest Hits"},
    )
    collection.remove()

    a1 = music_tag.load_file(AUDIO1)
    assert a1.info() == "artist: Backstreet Boys"

    a2 = music_tag.load_file(AUDIO2)
    assert a2.info() == "artist: Backstreet Boys"

    a3 = music_tag.load_file(AUDIO3)
    assert a3.info() == "artist: Backstreet Boys"


def test_collection_tagger_applies_specified_tags_but_does_not_override_existing():
    a1 = music_tag.load_file(AUDIO1)
    a1["title"] = "hello"
    a1["artist"] = "Backstreet Boys"
    a1.save()

    a2 = music_tag.load_file(AUDIO2)
    a2["artist"] = "Backstreet Boys"
    a2.save()

    a3 = music_tag.load_file(AUDIO3)
    a3["artist"] = "Backstreet Boys"
    a3["title"] = "foo"
    a3.save()

    collection = CollectionTagger(
        file_taggers=[
            FileTagger.from_file(AUDIO1),
            FileTagger.from_file(AUDIO2),
            FileTagger.from_file(AUDIO3),
        ],
        removed_tags=["title"],
        applied_tags={"genre": "Punk Rock", "album": "Backstreet Boys Greatest Hits"},
    )
    collection.apply()

    a1 = music_tag.load_file(AUDIO1)
    assert (
        a1.info()
        == """tracktitle: hello
artist: Backstreet Boys
album: Backstreet Boys Greatest Hits
genre: Punk Rock"""
    )

    # File name was set for title where not specified in tags or already assigned.
    a2 = music_tag.load_file(AUDIO2)
    assert (
        a2.info()
        == """tracktitle: a2.wav
artist: Backstreet Boys
album: Backstreet Boys Greatest Hits
genre: Punk Rock"""
    )

    a3 = music_tag.load_file(AUDIO3)
    assert (
        a3.info()
        == """tracktitle: foo
artist: Backstreet Boys
album: Backstreet Boys Greatest Hits
genre: Punk Rock"""
    )


def test_collection_tagger_assigns_track_numers_if_indicated():
    a1 = music_tag.load_file(AUDIO1)
    a1["title"] = "hello"
    a1["artist"] = "Backstreet Boys"
    a1.save()

    a2 = music_tag.load_file(AUDIO2)
    a2["artist"] = "Backstreet Boys"
    a2.save()

    a3 = music_tag.load_file(AUDIO3)
    a3["artist"] = "Backstreet Boys"
    a3["title"] = "foo"
    a3.save()

    collection = CollectionTagger(
        file_taggers=[
            FileTagger.from_file(AUDIO1),
            FileTagger.from_file(AUDIO2),
            FileTagger.from_file(AUDIO3),
        ],
        removed_tags=["title"],
        applied_tags={"genre": "Punk Rock", "album": "Backstreet Boys Greatest Hits"},
        assign_track_numbers=True,
    )
    collection.apply()

    a1 = music_tag.load_file(AUDIO1)
    assert (
        a1.info()
        == """tracktitle: hello
artist: Backstreet Boys
album: Backstreet Boys Greatest Hits
tracknumber: 1
totaltracks: 3
genre: Punk Rock"""
    )

    # File name was set for title where not specified in tags or already assigned.
    a2 = music_tag.load_file(AUDIO2)
    assert (
        a2.info()
        == """tracktitle: a2.wav
artist: Backstreet Boys
album: Backstreet Boys Greatest Hits
tracknumber: 2
totaltracks: 3
genre: Punk Rock"""
    )

    a3 = music_tag.load_file(AUDIO3)
    assert (
        a3.info()
        == """tracktitle: foo
artist: Backstreet Boys
album: Backstreet Boys Greatest Hits
tracknumber: 3
totaltracks: 3
genre: Punk Rock"""
    )


def test_collection_tagger_inherits_existing_tags_if_specified():
    a1 = music_tag.load_file(AUDIO1)
    a1["title"] = "hello"
    a1["artist"] = "Backstreet Boys"
    a1.save()

    # AUDIO2 is not tagged but will inherit relevant tags.

    a3 = music_tag.load_file(AUDIO3)
    a3["artist"] = "Backstreet Boys"
    a3["title"] = "foo"
    a3.save()

    collection = CollectionTagger(
        file_taggers=[
            FileTagger.from_file(AUDIO1),
            FileTagger.from_file(AUDIO2),
            FileTagger.from_file(AUDIO3),
        ],
        removed_tags=["title"],
        applied_tags={"genre": "Punk Rock", "album": "Backstreet Boys Greatest Hits"},
        assign_track_numbers=True,
        inherit_tags=True,
    )
    collection.apply()

    a1 = music_tag.load_file(AUDIO1)
    assert (
        a1.info()
        == """tracktitle: hello
artist: Backstreet Boys
album: Backstreet Boys Greatest Hits
tracknumber: 1
totaltracks: 3
genre: Punk Rock"""
    )

    # File name was set for title where not specified in tags or already assigned.
    a2 = music_tag.load_file(AUDIO2)
    assert (
        a2.info()
        == """tracktitle: a2.wav
artist: Backstreet Boys
album: Backstreet Boys Greatest Hits
tracknumber: 2
totaltracks: 3
genre: Punk Rock"""
    )

    a3 = music_tag.load_file(AUDIO3)
    assert (
        a3.info()
        == """tracktitle: foo
artist: Backstreet Boys
album: Backstreet Boys Greatest Hits
tracknumber: 3
totaltracks: 3
genre: Punk Rock"""
    )


def test_collection_tagger_applies_nothing_if_no_tags_or_assign_track_numbers():
    a1 = music_tag.load_file(AUDIO1)
    a1["title"] = "hello"
    a1["artist"] = "Backstreet Boys"
    a1.save()

    a2 = music_tag.load_file(AUDIO2)
    a2["artist"] = "Backstreet Boys"
    a2.save()

    a3 = music_tag.load_file(AUDIO3)
    a3["artist"] = "Backstreet Boys"
    a3["title"] = "foo"
    a3.save()

    collection = CollectionTagger(
        file_taggers=[
            FileTagger.from_file(AUDIO1),
            FileTagger.from_file(AUDIO2),
            FileTagger.from_file(AUDIO3),
        ],
        removed_tags=["title"],
        applied_tags={},
        assign_track_numbers=False,
    )
    collection.apply()

    a1 = music_tag.load_file(AUDIO1)
    assert (
        a1.info()
        == """tracktitle: hello
artist: Backstreet Boys"""
    )

    # File name was set for title where not specified in tags or already assigned.
    a2 = music_tag.load_file(AUDIO2)
    assert a2.info() == "artist: Backstreet Boys"

    a3 = music_tag.load_file(AUDIO3)
    assert (
        a3.info()
        == """tracktitle: foo
artist: Backstreet Boys"""
    )
