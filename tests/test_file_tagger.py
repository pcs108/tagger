import music_tag
import pytest

from tagger import FileTagger, FileTagsInfoPresenter, TagNameValidator

from . import AUDIO1, TEST_DATA


def test_file_tagger_from_file_for_valid_file():
    file_tagger = FileTagger.from_file(AUDIO1)
    assert file_tagger.tagger.filename == AUDIO1


def test_file_tagger_from_file_raises_error_for_invalid_file():
    txt_file = TEST_DATA / "invalid.txt"
    txt_file.write_text("")
    with pytest.raises(FileTagger.InvalidFile):
        FileTagger.from_file(txt_file)


def test_file_tagger_from_file_raises_error_for_directory():
    with pytest.raises(FileTagger.InvalidFile):
        FileTagger.from_file(TEST_DATA)


def test_file_tagger_file_name_property_returns_name_of_file():
    assert FileTagger.from_file(AUDIO1).file_name == "a1.wav"


def test_file_tagger_info_property_presents_info_about_file_and_tags():
    tagger = FileTagger.from_file(AUDIO1)
    assert (
        tagger.info
        == FileTagsInfoPresenter(
            file_name=tagger.file_name, tags_info=tagger.tagger.info()
        ).present()
    )


def test_file_tagger_get_tag_value_returns_file_tag_value_from_normalized_tag_name():
    a1 = music_tag.load_file(AUDIO1)
    a1["tracktitle"] = "I Want It That Way"
    a1.save()

    tagger = FileTagger.from_file(AUDIO1)
    assert tagger.get_tag_value("track_title") == "I Want It That Way"


def test_file_tagger_get_tag_value_raises_error_for_invalid_tag_name():
    tagger = FileTagger.from_file(AUDIO1)
    with pytest.raises(TagNameValidator.InvalidTagName):
        tagger.get_tag_value("foobar")


def test_file_tagger_tag_applies_tag_values_to_file():
    tagger = FileTagger.from_file(AUDIO1)
    tagger.tag({"title": "foo", "genre": "bar"})

    a1_tags = music_tag.load_file(AUDIO1)
    assert a1_tags.info() == "tracktitle: foo\ngenre: bar"


def test_file_tagger_remove_strips_specified_tags_from_file():
    a1_tags = music_tag.load_file(AUDIO1)
    a1_tags["title"] = "foo"
    a1_tags["genre"] = "bar"
    a1_tags["album"] = "hello"
    a1_tags.save()

    tagger = FileTagger.from_file(AUDIO1)
    tagger.remove(["genre", "album"])

    a1_tags = music_tag.load_file(AUDIO1)
    assert a1_tags.info() == "tracktitle: foo"
