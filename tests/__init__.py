from pathlib import Path

TESTS_DIR = Path(__file__).parent
AUDIO_FILE = TESTS_DIR / ".audio.wav"

TEST_DATA = TESTS_DIR / ".data"

AUDIO1 = TEST_DATA / "a1.wav"
AUDIO2 = TEST_DATA / "a2.wav"
AUDIO3 = TEST_DATA / "a3.wav"
