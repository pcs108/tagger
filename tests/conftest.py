import shutil

import pytest

from . import AUDIO1, AUDIO2, AUDIO3, AUDIO_FILE, TEST_DATA


@pytest.fixture(autouse=True)
def clean_up_files():
    def _remove():
        if TEST_DATA.exists():
            shutil.rmtree(TEST_DATA)

    _remove()
    TEST_DATA.mkdir()
    for path in [AUDIO1, AUDIO2, AUDIO3]:
        shutil.copyfile(AUDIO_FILE, path)
    yield
    _remove()
