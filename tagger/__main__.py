#!/usr/bin/env python3
from pathlib import Path
from typing import Tuple

import click

from . import CollectionTagger


def abort_with_error(message):
    click.echo(message)
    raise click.Abort()


@click.command()
@click.option("--info", "-i", is_flag=True)
@click.option("--inherit", is_flag=True)
@click.argument("directory", required=False)
@click.option("--file", "-f", multiple=True)
@click.option("--tag", "-t", multiple=True)
@click.option("--remove", "-r", multiple=True)
@click.option("--number", "-n", is_flag=True)
@click.option("--do-not-number", "-x", is_flag=True)
def tag(
    info: bool,
    inherit: bool,
    directory: str,
    file: Tuple[str],
    tag: Tuple[str],
    remove: Tuple[str],
    number: bool,
    do_not_number: bool,
) -> None:
    modifying_tags = tag or remove or number

    if not (info or modifying_tags):
        abort_with_error("Must display tag info or apply/remove tags")

    if modifying_tags and not (number or do_not_number):
        abort_with_error(
            "Must specify whether to assign track numbers or not when modifying tags"
        )

    if directory:
        if file:
            abort_with_error("Cannot specify directory and files")
        files = sorted(list(Path(directory).iterdir()))
    elif file:
        files = sorted([Path(f) for f in file])
    else:
        abort_with_error("Must specify directory or files")
        return

    collection = CollectionTagger.from_files_and_user_inputs(
        files=files,
        inherit_tags=inherit,
        applied_tags_input=tag,
        removed_tags_input=remove,
        assign_track_numbers=number,
    )

    collection.remove()
    collection.apply()

    if info:
        click.echo(collection.get_info())


def main() -> None:
    tag()


if __name__ == "__main__":
    main()
