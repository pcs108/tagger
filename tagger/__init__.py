import dataclasses
import typing
from copy import copy
from pathlib import Path

import music_tag
from mutagen import MutagenError

SAMPLE_AUDIO_FILE = Path(__file__).parent / ".sample.wav"
SAMPLE_AUDIO_FILE_TAGGER = music_tag.load_file(SAMPLE_AUDIO_FILE)


@dataclasses.dataclass(slots=True)
class TagNameValidator:
    class InvalidTagName(Exception):
        pass

    SAMPLE_VALUE = "108"

    tag: str

    def validate(self) -> bool:
        try:
            SAMPLE_AUDIO_FILE_TAGGER[self.tag] = TagNameValidator.SAMPLE_VALUE
        except KeyError:
            raise TagNameValidator.InvalidTagName(self.tag)
        return True


@dataclasses.dataclass(slots=True)
class TagValueValidator:
    class InvalidTagValue(Exception):
        pass

    tag: str
    value: typing.Any

    def validate(self) -> bool:
        try:
            SAMPLE_AUDIO_FILE_TAGGER[self.tag] = self.value
        except (KeyError, ValueError):
            raise TagValueValidator.InvalidTagValue(f"{self.tag}: {self.value!r}")
        return True


@dataclasses.dataclass(slots=True)
class TagNameParser:
    input: str

    def parse(self) -> str:
        tag = self.input.replace("_", "")
        TagNameValidator(tag).validate()
        return tag


@dataclasses.dataclass(slots=True)
class TagValueParser:
    class InvalidTagInfo(Exception):
        pass

    tag_value: typing.Dict[str, typing.Any]

    def __post_init__(self) -> None:
        if len(self.tag_value.items()) != 1:
            raise TagValueParser.InvalidTagInfo(self.tag_value)

    @property
    def tag(self) -> str:
        return list(self.tag_value.keys())[0]

    @property
    def value(self) -> typing.Any:
        return list(self.tag_value.values())[0]

    def parse(self) -> typing.Dict[str, typing.Union[str, int]]:
        tag = TagNameParser(self.tag).parse()
        TagValueValidator(tag=tag, value=self.value).validate()
        return {tag: self.value}


@dataclasses.dataclass(slots=True)
class TagInputParser:
    input: str

    def parse(self) -> typing.Dict[str, typing.Union[str, int]]:
        tag, value = self.input.split("=")
        return TagValueParser({tag: value}).parse()


@dataclasses.dataclass(slots=True)
class FileTagger:
    class InvalidFile(Exception):
        pass

    tagger: music_tag.AudioFile

    def __eq__(self, other) -> bool:
        return self.tagger.filename == other.tagger.filename

    @classmethod
    def from_file(cls, file: Path) -> "FileTagger":
        try:
            tagger = music_tag.load_file(file)
        except (NotImplementedError, MutagenError):
            raise FileTagger.InvalidFile(str(file))
        return cls(tagger)

    @property
    def file_name(self) -> str:
        return self.tagger.filename.name

    @property
    def info(self) -> str:
        return FileTagsInfoPresenter(
            file_name=self.file_name, tags_info=self.tagger.info()
        ).present()

    def get_tag_value(self, tag_name: str) -> typing.Union[str, int]:
        tag = TagNameParser(tag_name).parse()
        return self.tagger[tag].value

    def tag(self, tag_values: typing.Dict[str, typing.Union[str, int]]) -> None:
        for tag, value in tag_values.items():
            self.tagger[tag] = value
        self.tagger.save()

    def remove(self, tags: typing.Iterable[str]) -> None:
        for tag_name in tags:
            del self.tagger[tag_name]
        self.tagger.save()


@dataclasses.dataclass(slots=True, kw_only=True)
class FileTagsInfoPresenter:
    file_name: str
    tags_info: str

    def present(self) -> str:
        _tags_info = self.tags_info or "n/a"
        _tags_info = _tags_info.replace("tracktitle", "title")
        _tags_info = _tags_info.replace("tracknumber", "track")
        _tags_info = _tags_info.replace("totaltracks", "tracks")
        return f"[{self.file_name}] {_tags_info}"


@dataclasses.dataclass(slots=True, kw_only=True)
class CollectionTagger:
    file_taggers: typing.Iterable[FileTagger]
    applied_tags: typing.Dict[str, typing.Union[str, int]] = dataclasses.field(
        default_factory=dict
    )
    removed_tags: typing.Iterable[str] = dataclasses.field(default_factory=list)
    assign_track_numbers: bool = False
    inherit_tags: bool = False

    def __post_init__(self) -> None:
        if self.inherit_tags:
            for tagger in self.file_taggers:
                if (
                    "artist" in self.applied_tags
                    and "album" in self.applied_tags
                    and "genre" in self.applied_tags
                ):
                    break
                artist = tagger.get_tag_value("artist")
                if artist:
                    self.applied_tags["artist"] = artist
                album = tagger.get_tag_value("album")
                if album:
                    self.applied_tags["album"] = album
                genre = tagger.get_tag_value("genre")
                if genre:
                    self.applied_tags["genre"] = genre

    @classmethod
    def from_files_and_tags(
        cls,
        files: typing.Iterable[Path],
        applied_tags: typing.Optional[
            typing.Iterable[typing.Dict[str, typing.Any]]
        ] = None,
        removed_tags: typing.Optional[typing.Iterable[str]] = None,
        **kwargs,
    ) -> "CollectionTagger":
        applied_tags = applied_tags or {}
        removed_tags = removed_tags or []

        file_taggers = cls.build_file_taggers_for_files(files)
        applied_tags = cls.parse_applied_tags(applied_tags, parser=TagValueParser)
        removed_tags = cls.parse_removed_tags(removed_tags)

        return cls(
            file_taggers=file_taggers,
            applied_tags=applied_tags,
            removed_tags=removed_tags,
            **kwargs,
        )

    @classmethod
    def from_files_and_user_inputs(
        cls,
        files: typing.Iterable[Path],
        applied_tags_input: typing.Optional[typing.Iterable[str]] = None,
        removed_tags_input: typing.Optional[typing.Iterable[str]] = None,
        **kwargs,
    ) -> "CollectionTagger":
        applied_tags_input = applied_tags_input or []
        removed_tags_input = removed_tags_input or []

        file_taggers = cls.build_file_taggers_for_files(files)
        applied_tags = cls.parse_applied_tags(applied_tags_input, parser=TagInputParser)
        removed_tags = cls.parse_removed_tags(removed_tags_input)

        return cls(
            file_taggers=file_taggers,
            applied_tags=applied_tags,
            removed_tags=removed_tags,
            **kwargs,
        )

    @staticmethod
    def parse_applied_tags(
        tags: typing.Iterable[typing.Union[str, dict]], parser: typing.Any
    ) -> typing.Dict[str, str]:
        applied_tags = {}
        for tag in tags:
            tag_value = parser(tag).parse()
            applied_tags = {**applied_tags, **tag_value}
        return applied_tags

    @staticmethod
    def parse_removed_tags(tags: typing.Iterable[str]) -> typing.List[str]:
        return [TagNameParser(tag).parse() for tag in tags]

    @staticmethod
    def build_file_taggers_for_files(
        files: typing.Iterable[Path],
    ) -> typing.List[FileTagger]:
        file_taggers = []
        for file in files:
            try:
                file_tagger = FileTagger.from_file(file)
                file_taggers.append(file_tagger)
            except FileTagger.InvalidFile:
                pass
        return file_taggers

    def apply(self) -> None:
        if not (self.applied_tags or self.assign_track_numbers):
            return

        for track_number, file_tagger in enumerate(self.file_taggers, 1):
            file_tags = self._get_applied_file_tags(
                file_tagger=file_tagger, track_number=track_number
            )
            file_tagger.tag(file_tags)

    def _get_applied_file_tags(
        self, file_tagger: FileTagger, track_number: int
    ) -> dict:
        file_tags = copy(self.applied_tags)
        if self._title_is_not_specified(file_tagger):
            file_tags["title"] = file_tagger.file_name
        if self.assign_track_numbers:
            file_tags["tracknumber"] = track_number
            file_tags["totaltracks"] = self._number_of_total_tracks
        return file_tags

    def _title_is_not_specified(self, file_tagger) -> bool:
        return (
            "title" not in self.applied_tags and "tracktitle" not in self.applied_tags
        ) and not (
            file_tagger.get_tag_value("title")
            or file_tagger.get_tag_value("tracktitle")
        )

    @property
    def _number_of_total_tracks(self) -> int:
        return len(self.file_taggers)

    def get_info(self) -> str:
        return "\n".join([file_tagger.info for file_tagger in self.file_taggers])

    def remove(self) -> None:
        for file_tagger in self.file_taggers:
            file_tagger.remove(self.removed_tags)
